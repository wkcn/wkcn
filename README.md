### 大家好～

我叫吴侃，是18级中山大学与微软亚洲研究院联合培养直博生，预期**2023年**毕业。

我的研究方向是**计算机视觉模型压缩蒸馏**、**多模态大模型预训练**，我对**深度学习框架**研发也很感兴趣。

:star: 我正在求职，希望大家推荐地点在**深圳、成都**的工作，非常感谢！

**研究工作**：小模型预训练蒸馏[TinyViT](https://arxiv.org/pdf/2207.10666.pdf)、视觉Transformer模型压缩[MiniViT](https://openaccess.thecvf.com/content/CVPR2022/html/Zhang_MiniViT_Compressing_Vision_Transformers_With_Weight_Multiplexing_CVPR_2022_paper.html)、Transformer模型位置编码[iRPE](https://openaccess.thecvf.com/content/ICCV2021/html/Wu_Rethinking_and_Improving_Relative_Position_Encoding_for_Vision_Transformer_ICCV_2021_paper.html)、神经网络结构搜索[AutoFormerV2](https://proceedings.neurips.cc/paper/2021/file/48e95c45c8217961bf6cd7696d80d238-Paper.pdf)、[LightTrack](https://openaccess.thecvf.com/content/CVPR2021/papers/Yan_LightTrack_Finding_Lightweight_Neural_Networks_for_Object_Tracking_via_One-Shot_CVPR_2021_paper.pdf)和FP8混合精度训练框架[MS-AMP](https://github.com/azure/ms-amp)。

**开源项目贡献**：[`Apache/incubator-MXNet`](https://github.com/apache/incubator-mxnet/pulls?q=is%3Apr+author%3Awkcn+is%3Aclosed), [`Tencent/ncnn`](https://github.com/Tencent/ncnn/pulls?q=is%3Apr+author%3Awkcn+is%3Aclosed), [`OAID/Tengine`](https://github.com/OAID/Tengine/pulls?q=is%3Apr+author%3Awkcn+is%3Aclosed)等。

- 个人简历：[链接](https://github.com/wkcn/wkcn/releases/download/resume/KanWu-SYSU2023-Resume.pdf)|[链接2](https://gitee.com/wkcn/wkcn/releases/download/resume/KanWu-SYSU2023-Resume.pdf)
- 成果介绍：[链接](https://github.com/wkcn/wkcn/releases/download/resume/KanWu-SYSU2023-Achievement.pdf)|[链接2](https://gitee.com/wkcn/wkcn/releases/download/resume/KanWu-SYSU2023-Achievement.pdf)
- 谷歌学术：[链接](https://scholar.google.com/citations?user=sK4JUL4AAAAJ&hl=zh-CN)
- 联系邮箱：`wkcn AT live.cn`
